How to run.

1. git clone https://windanar@bitbucket.org/windanar/expose-react-to-outside.git
2. cd expose-react-to-outside
3. npm install
4. node node_modules\webpack\bin\webpack.js --config webpack.conf.js --output-path 'dist'
5. copy and paste ./dist/co_react_components_bundle.js to ./public (replace the existing)
6. npm start

Refer https://bitbucket.org/windanar/dynamic-render-lib/src/master/ for dynamic-render-lib implementaiton


Execute below to generate the exposed library.
node node_modules\webpack\bin\webpack.js --config webpack.conf.js --output-path 'dist'

set minimize to true or false in webpack.conf.js

optimization: {

        minimize: false

},

References:
https://sdk.gooddata.com/gooddata-ui/docs/4.1.1/ht_use_react_components_with_vanilla_js.html
https://github.com/gooddata/ui-sdk-examples/tree/master/vanillajs
