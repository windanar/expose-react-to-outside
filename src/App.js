import React from 'react';
import './App.css';
import ExampleComponent from '@win-test/win-react-comp-lib'

function App() {
  return (
    <div className="App">
      <ExampleComponent text='Hi' />
    </div>
  );
}

export default App;
