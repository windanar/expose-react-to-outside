const path = require('path');

module.exports = {
    entry: './exporter.js',
    optimization: {
        minimize: false
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'co_react_components_bundle.js',
        library: 'CO',
        libraryTarget: 'window'
    }
}; 
